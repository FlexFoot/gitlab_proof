-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Hi. This is a signature profile with proofs for Flex Foot

Verify this profile at
https://keyoxide.org/sig

proof=https://gitlab.torproject.org/FlexFoot/gitlab_proof
proof=https://codeberg.org/FlexFoot/gitea_proof
-----BEGIN PGP SIGNATURE-----

iIoEARYIADIWIQToUkClTvHesG2EQj2mnzbMoDg2UwUCZMCLKBQcZmxleGZvb3RA
cG9zdGVvLm5ldAAKCRCmnzbMoDg2U3RRAP9FI+A+ymxkT6tl1RR0BatojeQT+F+x
TEDwHHivZk9OvAD8DPpEvkIS94iWgkxOWAjeXkdsio15vXFE+U3EY1+ZhQo=
=crc9
-----END PGP SIGNATURE-----
